package com.adeoye.pixel;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;

import com.adeoye.pixel.model.UserObject;
import com.adeoye.pixel.util.Sync;
import com.google.gson.Gson;

public class ActivitySplash extends AppCompatActivity {
    private static final int SPLASH_TIME_OUT = 1000;
    private final String ERROR_CONNECTION_MESSAGE = "The Server couldn't be reached\nPlease ensure you are connected to the internet";
    private Context context;
    public UserObject user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.activity_splash);

        context = this;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        String USER = prefs.getString("USER", "");
        Log.e("SaleSniperLogin", "user: " + USER);
        user = new Gson().fromJson(USER, UserObject.class);
        if (user == null) {
            user = UserObject.getInstance();
        }

        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (isOnline()) {
                    AfterSplash();
                    return;
                }
                ShowAlert(ERROR_CONNECTION_MESSAGE, "Error");
            }
        }, SPLASH_TIME_OUT);

    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


    private void AfterSplash() {
        if (!TextUtils.isEmpty(user.getAccessToken())) {

            Intent intent = new Intent(ActivitySplash.this, MainActivity.class);
            startActivity(intent);
            finish();




            //new Sync(context).DoLogin(user.getEmail(), user.getPassword());
            return;
        }
        Intent intent = new Intent(ActivitySplash.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public void ShowAlert(String message, String title) {
        try {
            AlertDialog.Builder alert = new AlertDialog.Builder(context);
            alert.setTitle(title);
            alert.setMessage(message);
            alert.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (!isOnline()) {
                        ShowAlert(ERROR_CONNECTION_MESSAGE, "Error");
                        return;
                    }
                    new Sync(context).DoLogin(user.getEmail(), user.getPassword());
                }
            }).setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                }
            }).setCancelable(false);
            alert.setIcon(R.drawable.ic_error_black);
            alert.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
