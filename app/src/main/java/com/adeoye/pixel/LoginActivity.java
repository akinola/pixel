package com.adeoye.pixel;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.adeoye.pixel.model.LoginGoodResponse;
import com.adeoye.pixel.model.UserObject;
import com.adeoye.pixel.util.Sync;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    private final String ERROR_CONNECTION_MESSAGE = "Server couldn't be reached";
    private final String LOGIN_ENDPOINT = "http://mgic-sers.remadelive.com/oauth/token";
    private Context context;
    private Gson gson;
    private SharedPreferences prefs;
    private boolean showPassword;
    private TextView txtForgotPassword;
    private UserObject user;
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    //private AutoCompleteTextView mEmailView;
    private EditText mUsername;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = this;
        gson = new Gson();
        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        String USER = this.prefs.getString("USER", "");
        Log.e("SaleSniperLogin", "user: " + USER);
        user = gson.fromJson(USER, UserObject.class);
        if (user == null) {
            user = UserObject.getInstance();
        }

        this.mUsername = ((EditText) findViewById(R.id.username));
        this.txtForgotPassword = ((TextView) findViewById(R.id.txtForgotPassword));

        this.mUsername.setText(user.getEmail());

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mPasswordView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                //final int DRAWABLE_LEFT = 0;
                //final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                //final int DRAWABLE_BOTTOM = 3;

                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    try {
                        //int g = mPasswordView.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width();

                        //if(event.getRawX() >= (mPasswordView .getRight() - mPasswordView.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        if (motionEvent.getX() >= (mPasswordView.getRight() - mPasswordView.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width() - 10)) {
                            //your action here
                            if (showPassword) {
                                mPasswordView.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                                showPassword = false;
                            } else {
                                mPasswordView.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                                motionEvent.setAction(MotionEvent.ACTION_CANCEL);//use this to prevent the keyboard from coming up
                                showPassword = true;
                            }
                            return true;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return false;
            }
        });


        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mUsername.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mUsername.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mUsername.setError(getString(R.string.error_field_required));
            focusView = mUsername;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mUsername.setError(getString(R.string.error_invalid_email));
            focusView = mUsername;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask(email, password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private OkHttpClient client;
        public String errorMessage;
        public String res;
        public int resCode;
        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
            client = new OkHttpClient.Builder()
                    .connectTimeout(15, TimeUnit.SECONDS)
                    .writeTimeout(15, TimeUnit.SECONDS)
                    .readTimeout(15, TimeUnit.SECONDS)
                    .build();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                RequestBody formBody = new FormBody.Builder()
                        .add("username", mEmail)
                        .add("password", mPassword)
                        .add("client_id", "2")
                        .add("grant_type", "password")
                        .add("client_secret", "K9ShWBYJvWvOL57PfX6Q6PJQr0vr8aAmTz7RlHDK")
                        .build();

                Request request = new Request.Builder()
                        .post(formBody)
                        .url(LOGIN_ENDPOINT)
                        .build();

                //retrieve response
                Response response = client.newCall(request).execute();

                resCode = response.code();
                res = response.body().string();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                resCode = -1;
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                user.setEmail(mEmail);
                user.setPassword(mPassword);

                LoginGoodResponse gResponse = gson.fromJson(res, LoginGoodResponse.class);
                if (gResponse != null) {
                    user.setAccessToken(gResponse.getAccess_token());
                    user.setRefreshToken(gResponse.getRefresh_token());
                }

                String USER = gson.toJson(user);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("USER", USER);
                editor.apply();

                Sync sync = new Sync(context);
                sync.GetGroups();
                sync.GetUploadedFiles();
                Intent intent = new Intent(context, MainActivity.class);
                startActivity(intent);

                finish();
            } else {
                ShowAlert(this.errorMessage, "Error");
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }

        public void ShowAlert(String message, String title) {
            try {
                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setTitle(title);
                alert.setMessage(message);
                alert.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (!isOnline()) {
                            ShowAlert(ERROR_CONNECTION_MESSAGE, "Error");
                            return;
                        }
                        attemptLogin();
                    }
                });
                alert.setIcon(R.drawable.ic_error_black);
                alert.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}

