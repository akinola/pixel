package com.adeoye.pixel;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.adeoye.pixel.adapter.DocumentsRecyclerAdapter;
import com.adeoye.pixel.adapter.GridAutofitLayoutManager;
import com.adeoye.pixel.adapter.UploadedDocumentsRecyclerAdapter;
import com.adeoye.pixel.model.DocumentObject;
import com.adeoye.pixel.model.FileObject;
import com.adeoye.pixel.model.GroupObject;
import com.adeoye.pixel.model.UploadedFileObject;
import com.adeoye.pixel.model.UserObject;
import com.adeoye.pixel.util.MyDBHandler;
import com.adeoye.pixel.util.OnBackPressedListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        PicturesFragment.OnFragmentInteractionListener {

    public static RecyclerView.Adapter adapter;
    public static RecyclerView.Adapter adapterUploaded;
    //private static RecyclerView.Adapter pixAdapter;

    public static List<DocumentObject> documentObjectsList;
    //public static List<AlbumObject> uploadedAlbumObjectList;
    public static List<UploadedFileObject> uploadedFileObjects;

    private static MyDBHandler myDBHandler;
    public static UserObject user;
    private Gson gson;
    private SharedPreferences prefs;
    private static Context context;

    private int currentAlbumPosition = -1;
    private final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private final int PICK_FILE_RESULT = 300;


    private static String LAST_TITLE = "Pixel";
    private static final int TIME_INTERVAL = 2000;
    private long mBackPressed;

    private String root;
    private File myDir;
    private String imageFileName;
    //private String mCurrentPhotoPath;
    File mImageCaptureUri;


    private DocumentsFragment documentsFragment;
    private TabularDocumentsFragment tabularDocumentsFragment;
    private TabularFilesFragment tabularFilesFragment;
    //private PicturesFragment picturesFragment;
    private UploadedFilesFragment uploadedFilesFragment;
    private HelpFragment helpFragment;
    private FeedbackFragment feedbackFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context = this;
        myDBHandler = MyDBHandler.getInstance(context);
        gson = new Gson();
        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        try {
            setStatusBar();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String USER = prefs.getString("USER", "");
        user = gson.fromJson(USER, UserObject.class);
        if (user == null) {
            user = UserObject.getInstance();
        }

        root = Environment.getExternalStorageDirectory().toString();
        myDir = new File(root + "/Pixel");

        //noinspection ResultOfMethodCallIgnored
        myDir.mkdirs();

        if (documentObjectsList == null || uploadedFileObjects == null) {
            populateList();
        }

        if (savedInstanceState == null) {
            /*documentsFragment = DocumentsFragment.newInstance(1);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.content_main, documentsFragment, "DOCUMENTS")
                    //.addToBackStack("OUTLETS")
                    .commit();*/

            tabularDocumentsFragment = TabularDocumentsFragment.newInstance(1);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.content_main, tabularDocumentsFragment, "DOCUMENTS")
                    //.addToBackStack("OUTLETS")
                    .commit();
        } else {
            /*documentsFragment = (DocumentsFragment) getSupportFragmentManager()
                    .findFragmentByTag("DOCUMENTS");*/

            tabularDocumentsFragment = (TabularDocumentsFragment) getSupportFragmentManager()
                    .findFragmentByTag("DOCUMENTS");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void setStatusBar() {
        //Requires APi 21
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(context.getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    /*Fragments*/
    public static class DocumentsFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";
        static CardView c;
        private SharedPreferences prefs;

        public DocumentsFragment() {
        }

        public static DocumentsFragment newInstance(int sectionNumber) {
            DocumentsFragment fragment = new DocumentsFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_albums, container, false);
            /*Bundle extras = getArguments();
            int pos = 0;
            if (extras != null) {
                pos = extras.getInt(ARG_SECTION_NUMBER);
            }*/

            prefs = PreferenceManager.getDefaultSharedPreferences(context);
            try {
                //noinspection ConstantConditions
                ((AppCompatActivity) context).getSupportActionBar().setTitle("Pixel");
            } catch (Exception e) {
                e.printStackTrace();
            }

            LAST_TITLE = "Pixel";

            RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerViewAlbum);
            adapter = new DocumentsRecyclerAdapter(context);

            GridAutofitLayoutManager gridLayoutManager = new GridAutofitLayoutManager(context, 190);

            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.setAdapter(adapter);

            FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);

            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ShowNewAlbumCreationDialog();
                }
            });

            c = (CardView) rootView.findViewById(R.id.cardViewInstruction);
            DoListCheck();

            return rootView;
        }

        public static void DoListCheck() {
            if (documentObjectsList.size() > 0) {
                c.setVisibility(View.GONE);
            } else {
                c.setVisibility(View.VISIBLE);
            }
        }

        private void ShowNewAlbumCreationDialog() {
            final AlertDialog.Builder addAlbumDialog = new AlertDialog.Builder(context);
            addAlbumDialog.setMessage("Add Document");

            String group = prefs.getString("GROUPS", "");
            Type listType = new TypeToken<List<GroupObject>>() {
            }.getType();
            List<GroupObject> groupObjectList = new Gson().fromJson(group, listType);


            List<String> spinnerItems = new ArrayList<>();
            if (groupObjectList == null) {
                spinnerItems = Arrays.asList(getResources().getStringArray(R.array.album_groups));
            } else {
                for (GroupObject gg : groupObjectList) {
                    spinnerItems.add(gg.getName());
                }
            }

            View v = View.inflate(context, R.layout.add_album_dialog, null);
            final EditText editAlbumName = (EditText) v.findViewById(R.id.editAlbumName);
            final EditText editAlbumDescription = (EditText) v.findViewById(R.id.editAlbumDescription);
            final EditText editAlbumTags = (EditText) v.findViewById(R.id.editAlbumTags);
            final Spinner spinnerAlbumGroup = (Spinner) v.findViewById(R.id.spinnerAlbumGroup);
            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, spinnerItems);
            spinnerAlbumGroup.setAdapter(spinnerAdapter);


            Button btnCreateAlbum = (Button) v.findViewById(R.id.btnCreateAlbum);
            Button btnCancel = (Button) v.findViewById(R.id.btnCancel);

            addAlbumDialog.setView(v);
            final Dialog dialog = addAlbumDialog.create();

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            btnCreateAlbum.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String albumName = editAlbumName.getText().toString();
                    String albumDescription = editAlbumDescription.getText().toString();
                    String albumTags = editAlbumTags.getText().toString();
                    String albumGroup = spinnerAlbumGroup.getSelectedItem().toString();

                    DocumentObject d = new DocumentObject();
                    d.setName(albumName);
                    d.setDescription(albumDescription);
                    d.setGroup(albumGroup);
                    d.setTags(albumTags);


                    Log.e("Pixel", "Document: " + d.toString());
                    //documentObjectsList.add(a);
                    boolean t = myDBHandler.InsertDocument(d);
                    Log.e("Pixel", "album insertion db: " + t);
                    documentObjectsList = myDBHandler.GetUnUploaded();
                    adapter.notifyDataSetChanged();
                    DoListCheck();
                    dialog.dismiss();
                }
            });


            dialog.show();
        }
    }


    public static class TabularDocumentsFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";
        static CardView c;
        private SharedPreferences prefs;
        private static TableLayout tableLayoutDocuments;

        public TabularDocumentsFragment() {
        }

        public static TabularDocumentsFragment newInstance(int sectionNumber) {
            TabularDocumentsFragment fragment = new TabularDocumentsFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_albums2, container, false);
            /*Bundle extras = getArguments();
            int pos = 0;
            if (extras != null) {
                pos = extras.getInt(ARG_SECTION_NUMBER);
            }*/

            prefs = PreferenceManager.getDefaultSharedPreferences(context);
            try {
                //noinspection ConstantConditions
                ((AppCompatActivity) context).getSupportActionBar().setTitle("Pixel");
            } catch (Exception e) {
                e.printStackTrace();
            }

            LAST_TITLE = "Pixel";

            tableLayoutDocuments = (TableLayout) rootView.findViewById(R.id.tableLayoutDocuments);
            LoadProductsTable();

            FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);

            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ShowNewAlbumCreationDialog();
                }
            });

            c = (CardView) rootView.findViewById(R.id.cardViewInstruction);
            DoListCheck();

            return rootView;
        }

        private static void LoadProductsTable() {

            tableLayoutDocuments.removeAllViewsInLayout();
            TableRow tableHeader = (TableRow) ((AppCompatActivity) context).getLayoutInflater().inflate(R.layout.tabular_document_item_header, null);
            tableLayoutDocuments.addView(tableHeader);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            View vh = new View(context);
            vh.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1));
            vh.setBackgroundColor(Color.rgb(150, 200, 250));
            tableLayoutDocuments.addView(vh);

            //final List<ProductInfoObject> productInfoObjects = OutletListRecyclerAdapter.outlets.get(pos).getProductInfoObjectList();
            int l = documentObjectsList.size();
            for (int i = 0; i < l; i++) {
                final int ii = i;
                TableRow row = (TableRow) View.inflate(context, R.layout.tabular_document_item_layout, null);
                row.setClickable(true);
                if (i % 2 == 0) {
                    row.setBackgroundResource(R.drawable.document_table_background_even);
                } else {
                    row.setBackgroundResource(R.drawable.document_table_background_odd);
                }


                row.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        TabularFilesFragment tabularFilesFragment = TabularFilesFragment.newInstance(ii);
                        FragmentTransaction fragmentTransaction = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.add(R.id.content_main, tabularFilesFragment, "FILES")
                                .addToBackStack("FILES")
                                .commit();
                    }
                });

                row.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        return true;
                    }
                });


                TextView textViewDocumentName = (TextView) row.findViewById(R.id.textViewDocumentName);
                TextView textViewDocumentCount = (TextView) row.findViewById(R.id.textViewDocumentCount);
                TextView textViewDocumentSize = (TextView) row.findViewById(R.id.textViewDocumentSize);
                //TextView textViewDocumentAction = (TextView)row.findViewById(R.id.textViewDocumentAction);

                textViewDocumentName.setText(documentObjectsList.get(i).getName());
                List<FileObject> files = documentObjectsList.get(i).getCollections();
                int x = files.size();
                textViewDocumentCount.setText(String.valueOf(x));

                int size = 0;
                for (FileObject f:files) {
                    File f1 = new File(f.getPix());
                    if (f1.exists()){
                        size += f1.length();
                    }
                }

                double a = (double) size/1000000.0;
                double sizeD = (double) Math.round(a * 100) / 100;

                textViewDocumentSize.setText(String.valueOf(sizeD)+"Mb");

                tableLayoutDocuments.addView(row);

                if (i < l - 1) {
                    View vh2 = new View(context);
                    vh2.setBackgroundColor(Color.rgb(200, 200, 200));
                    vh2.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1));
                    tableLayoutDocuments.addView(vh2);
                }
            }
        }

        public static void DoListCheck() {
            if (documentObjectsList.size() > 0) {
                c.setVisibility(View.GONE);
            } else {
                c.setVisibility(View.VISIBLE);
            }
        }

        private void ShowNewAlbumCreationDialog() {
            final AlertDialog.Builder addAlbumDialog = new AlertDialog.Builder(context);
            addAlbumDialog.setMessage("Add Document");

            String group = prefs.getString("GROUPS", "");
            Type listType = new TypeToken<List<GroupObject>>() {
            }.getType();
            List<GroupObject> groupObjectList = new Gson().fromJson(group, listType);


            List<String> spinnerItems = new ArrayList<>();
            if (groupObjectList == null) {
                spinnerItems = Arrays.asList(getResources().getStringArray(R.array.album_groups));
            } else {
                for (GroupObject gg : groupObjectList) {
                    spinnerItems.add(gg.getName());
                }
            }

            View v = View.inflate(context, R.layout.add_album_dialog, null);
            final EditText editAlbumName = (EditText) v.findViewById(R.id.editAlbumName);
            final EditText editAlbumDescription = (EditText) v.findViewById(R.id.editAlbumDescription);
            final EditText editAlbumTags = (EditText) v.findViewById(R.id.editAlbumTags);
            final Spinner spinnerAlbumGroup = (Spinner) v.findViewById(R.id.spinnerAlbumGroup);
            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, spinnerItems);
            spinnerAlbumGroup.setAdapter(spinnerAdapter);


            Button btnCreateAlbum = (Button) v.findViewById(R.id.btnCreateAlbum);
            Button btnCancel = (Button) v.findViewById(R.id.btnCancel);

            addAlbumDialog.setView(v);
            final Dialog dialog = addAlbumDialog.create();

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            btnCreateAlbum.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String albumName = editAlbumName.getText().toString();
                    String albumDescription = editAlbumDescription.getText().toString();
                    String albumTags = editAlbumTags.getText().toString();
                    String albumGroup = spinnerAlbumGroup.getSelectedItem().toString();

                    DocumentObject d = new DocumentObject();
                    d.setName(albumName);
                    d.setDescription(albumDescription);
                    d.setGroup(albumGroup);
                    d.setTags(albumTags);


                    Log.e("Pixel", "Document: " + d.toString());
                    //documentObjectsList.add(a);
                    boolean t = myDBHandler.InsertDocument(d);
                    Log.e("Pixel", "album insertion db: " + t);
                    documentObjectsList = myDBHandler.GetUnUploaded();
                    //adapter.notifyDataSetChanged();
                    LoadProductsTable();
                    DoListCheck();
                    dialog.dismiss();
                }
            });


            dialog.show();
        }
    }


    public static class TabularFilesFragment extends Fragment implements OnBackPressedListener {

        private static final String DOCUMENT_INDEX = "DOCUMENT_INDEX";
        static CardView c;
        private SharedPreferences prefs;
        private static TableLayout tableLayoutFiles;
        int index;

        public TabularFilesFragment() {
        }

        public static TabularFilesFragment newInstance(int documentIndex) {
            TabularFilesFragment fragment = new TabularFilesFragment();
            Bundle args = new Bundle();
            args.putInt(DOCUMENT_INDEX, documentIndex);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_albums_files, container, false);
            Bundle extras = getArguments();

            if (extras != null) {
                index = extras.getInt(DOCUMENT_INDEX);
            }

            prefs = PreferenceManager.getDefaultSharedPreferences(context);
            try {
                //noinspection ConstantConditions
                ((AppCompatActivity) context).getSupportActionBar().setTitle("Pixel");
            } catch (Exception e) {
                e.printStackTrace();
            }

            LAST_TITLE = "Pixel";

            tableLayoutFiles = (TableLayout) rootView.findViewById(R.id.tableLayoutFiles);
            LoadFilesTable(index);

            FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);

            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ShowNewFileCreationDialog(index);
                }
            });

            c = (CardView) rootView.findViewById(R.id.cardViewInstruction);
            DoListCheck();

            return rootView;
        }

        private static void LoadFilesTable(int index) {

            SimpleDateFormat format = new SimpleDateFormat("MMMM dd yyyy hh:mm aaa", Locale.ENGLISH);
            List<FileObject> files = documentObjectsList.get(index).getCollections();

            tableLayoutFiles.removeAllViewsInLayout();
            TableRow tableHeader = (TableRow) ((AppCompatActivity) context).getLayoutInflater().inflate(R.layout.tabular_files_item_header, null);
            tableLayoutFiles.addView(tableHeader);

            //LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            View vh = new View(context);
            vh.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1));
            vh.setBackgroundColor(Color.rgb(150, 200, 250));
            tableLayoutFiles.addView(vh);

            //final List<ProductInfoObject> productInfoObjects = OutletListRecyclerAdapter.outlets.get(pos).getProductInfoObjectList();
            int l = files.size();
            for (int i = 0; i < l; i++) {
                //final int ii = i;
                TableRow row = (TableRow) View.inflate(context, R.layout.tabular_files_item_layout, null);
                row.setClickable(true);
                if (i % 2 == 0) {
                    row.setBackgroundResource(R.drawable.document_table_background_even);
                } else {
                    row.setBackgroundResource(R.drawable.document_table_background_odd);
                }


                row.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });


                TextView textViewFileName = (TextView) row.findViewById(R.id.textViewFileName);
                TextView textViewFileDate = (TextView) row.findViewById(R.id.textViewFileDate);

                File f = new File(files.get(i).getPix());
                if (f.exists()) {
                    textViewFileName.setText(f.getName());

                    Date lastModDate = new Date(f.lastModified());
                    String x = format.format(lastModDate);
                    textViewFileDate.setText(x);

                    tableLayoutFiles.addView(row);
                } else {
                    continue;
                }

                if (i < l - 1) {
                    View vh2 = new View(context);
                    vh2.setBackgroundColor(Color.rgb(200, 200, 200));
                    vh2.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1));
                    tableLayoutFiles.addView(vh2);
                }
            }
        }

        public static void DoListCheck() {
            if (documentObjectsList.size() > 0) {
                c.setVisibility(View.GONE);
            } else {
                c.setVisibility(View.VISIBLE);
            }
        }

        private void ShowNewFileCreationDialog(final int index) {
            final AlertDialog.Builder addFileDialog = new AlertDialog.Builder(context);
            addFileDialog.setTitle("Add File");
            String[] actions = {"Add Files From Gallery", "Take a Picture"};


            addFileDialog.setItems(actions, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (i == 0) {
                        ((MainActivity) context).PickFileFromGallery(index);
                    } else if (i == 1) {
                        ((MainActivity) context).TakePicture(index);
                    }
                }
            });

            addFileDialog.show();

        }

        @Override
        public void onBackPressed() {
            Log.e("UserObj", "We pressed back");
            if (MainActivity.adapter != null) {
                MainActivity.adapter.notifyDataSetChanged();
            }

            try {
                //noinspection ConstantConditions
                ((AppCompatActivity) context).getSupportActionBar().setTitle("Pixel");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }


    private void PickFileFromGallery(int index) {
        currentAlbumPosition = index;
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        // Start the Intent
        startActivityForResult(galleryIntent, PICK_FILE_RESULT);
    }


    public static class UploadedFilesFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";
        private static SwipeRefreshLayout swipeRefreshLayout;

        public UploadedFilesFragment() {
        }

        public static UploadedFilesFragment newInstance(int sectionNumber) {
            UploadedFilesFragment fragment = new UploadedFilesFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_uploaded_albums, container, false);
            //Bundle extras = getArguments();
            /*int pos = 0;
            if (extras != null) {
                pos = extras.getInt(ARG_SECTION_NUMBER);
            }*/

            try {
                //noinspection ConstantConditions
                ((AppCompatActivity) context).getSupportActionBar().setTitle("Uploaded Documents");
            } catch (Exception e) {
                e.printStackTrace();
            }

            LAST_TITLE = "Uploaded Documents";

            RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerViewAlbum);
            adapterUploaded = new UploadedDocumentsRecyclerAdapter(context);

            GridAutofitLayoutManager gridLayoutManager = new GridAutofitLayoutManager(context, 130);
            swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);

            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.setAdapter(adapterUploaded);

            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    adapterUploaded.notifyDataSetChanged();
                }
            });

            return rootView;
        }

        public static void showSwipeRefresh(boolean value) {
            swipeRefreshLayout.setRefreshing(value);
        }
    }

    public static class HelpFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";

        public HelpFragment() {
        }

        public static HelpFragment newInstance(int sectionNumber) {
            HelpFragment fragment = new HelpFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_help, container, false);
            //Bundle extras = getArguments();

            try {
                //noinspection ConstantConditions
                ((AppCompatActivity) context).getSupportActionBar().setTitle("Help");
            } catch (Exception e) {
                e.printStackTrace();
            }

            LAST_TITLE = "Help";
            return rootView;
        }
    }

    public static class FeedbackFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";

        public FeedbackFragment() {
        }

        public static FeedbackFragment newInstance(int sectionNumber) {
            FeedbackFragment fragment = new FeedbackFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_feedback, container, false);
            //Bundle extras = getArguments();

            try {
                //noinspection ConstantConditions
                ((AppCompatActivity) context).getSupportActionBar().setTitle("Feedback");
            } catch (Exception e) {
                e.printStackTrace();
            }

            LAST_TITLE = "Feedback";
            return rootView;
        }
    }


    private void populateList() {
        documentObjectsList = new ArrayList<>();
        //uploadedAlbumObjectList = new ArrayList<>();

        documentObjectsList = myDBHandler.GetUnUploaded();
        //uploadedAlbumObjectList = myDBHandler.GetUploadedAlbums();
    }

    public void TakePicture(int albumPosition) {
        currentAlbumPosition = albumPosition;
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            //Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
                Toast.makeText(MainActivity.this, "Couldn't create Photo file!! Couldn't write to storage device!!", Toast.LENGTH_SHORT).show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        }
    }

    private File createImageFile() throws IOException {
        try {
            int dumIndex = documentObjectsList.get(currentAlbumPosition).getCollections().size();
            imageFileName = String.format(Locale.ENGLISH, "%s-%03d", documentObjectsList.get(currentAlbumPosition).getName(), dumIndex);

            File dummyFile = new File(myDir + "/" + imageFileName);
            String[] ext = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l"};
            int l = ext.length;
            int t = 0;
            while (dummyFile.exists() && t < l) {
                imageFileName = String.format(Locale.ENGLISH, "%s-%03d%s", documentObjectsList.get(currentAlbumPosition).getName(), dumIndex, ext[t]);
                dummyFile = new File(myDir + "/" + imageFileName);
                t++;
            }

            File image = File.createTempFile(this.imageFileName, ".jpg", myDir);

            //mCurrentPhotoPath = ("file:" + image.getAbsolutePath());
            imageFileName = image.getName();
            return image;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
                try {
                    mImageCaptureUri = new File(myDir + "/" + imageFileName);
                    if (mImageCaptureUri.exists()) {

                        try {
                            FileObject p = new FileObject();
                            p.setPix(mImageCaptureUri.getAbsolutePath());
                            p.setUploadStatus(false);

                            documentObjectsList
                                    .get(currentAlbumPosition)
                                    .getCollections()
                                    .add(p);

                            myDBHandler.UpdateDocument(documentObjectsList.get(currentAlbumPosition));
                            documentObjectsList = myDBHandler.GetUnUploaded();

                            /*if (adapter != null) {
                                adapter.notifyDataSetChanged();
                            }*/

                            /*if (PicturesFragment.pixAdapter != null) {
                                //pixAdapter.notifyDataSetChanged();
                                PicturesFragment.pixAdapter.notifyItemInserted(documentObjectsList
                                        .get(currentAlbumPosition)
                                        .getCollections().size() - 1);
                            }*/

                            TabularFilesFragment.LoadFilesTable(currentAlbumPosition);

                            TabularDocumentsFragment.LoadProductsTable();


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            if (requestCode == PICK_FILE_RESULT && data!=null){
                try {
                    // Get the Image from data

                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };

                    // Get the cursor
                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();

                    Log.e("PIXEL","picked file path: "+picturePath);

                    FileObject p = new FileObject();
                    p.setPix(picturePath);
                    p.setUploadStatus(false);

                    documentObjectsList
                            .get(currentAlbumPosition)
                            .getCollections()
                            .add(p);

                    myDBHandler.UpdateDocument(documentObjectsList.get(currentAlbumPosition));
                    documentObjectsList = myDBHandler.GetUnUploaded();

                    TabularFilesFragment.LoadFilesTable(currentAlbumPosition);
                    TabularDocumentsFragment.LoadProductsTable();

                /*//ImageView imgView = (ImageView) findViewById(R.id.imgView);
                // Set the Image in ImageView after decoding the String
                imgView.setImageBitmap(BitmapFactory
                        .decodeFile(imgDecodableString));*/
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("PIXEL","Some error occurred during file pick");
                }
            }
        }
    }


    @Override
    public void onBackPressed() {
        /*DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                    super.onBackPressed();
                } else {
                    Toast.makeText(getApplicationContext(), "Press the Back button again to exit", Toast.LENGTH_SHORT).show();
                    mBackPressed = System.currentTimeMillis();
                }
            } else {

                List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
                if (fragmentList != null) {
                    //TODO: Perform your logic to pass back press here
                    for (Fragment fragment : fragmentList) {
                        if (fragment instanceof OnBackPressedListener) {
                            ((OnBackPressedListener) fragment).onBackPressed();
                        }
                    }
                }

                super.onBackPressed();
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_logout) {
            user.setAccessToken("");
            String USER = this.gson.toJson(user);
            SharedPreferences.Editor editor = this.prefs.edit();
            editor.putString("USER", USER);
            editor.apply();

            finish();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return true;
        }

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (id == R.id.nav_documents) {
            /*if (documentsFragment == null) {
                documentsFragment = DocumentsFragment.newInstance(1);
            }
            fragmentTransaction.replace(R.id.content_main, documentsFragment, "DOCUMENTS");*/

            if (tabularDocumentsFragment == null) {
                tabularDocumentsFragment = TabularDocumentsFragment.newInstance(1);
            }
            fragmentTransaction.replace(R.id.content_main, tabularDocumentsFragment, "DOCUMENTS");
        } else if (id == R.id.nav_uploaded) {
            if (uploadedFilesFragment == null) {
                uploadedFilesFragment = UploadedFilesFragment.newInstance(1);
            }
            fragmentTransaction.replace(R.id.content_main, uploadedFilesFragment, "UPLOADED-FILES");
        } else if (id == R.id.nav_account) {
            //startActivity(new Intent(context, ActivityProfile.class));
        } else if (id == R.id.nav_help) {
            if (helpFragment == null) {
                helpFragment = HelpFragment.newInstance(1);
            }
            fragmentTransaction.replace(R.id.content_main, helpFragment, "HELP");
        } else if (id == R.id.nav_feedback) {
            if (feedbackFragment == null) {
                feedbackFragment = FeedbackFragment.newInstance(1);
            }
            fragmentTransaction.replace(R.id.content_main, feedbackFragment, "FEEDBACK");
        }

        fragmentTransaction.commit();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
