package com.adeoye.pixel;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adeoye.pixel.adapter.FilesRecyclerAdapter;
import com.adeoye.pixel.adapter.GridAutofitLayoutManager;
import com.adeoye.pixel.util.OnBackPressedListener;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PicturesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PicturesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PicturesFragment extends Fragment implements OnBackPressedListener {

    int pos = 0;
    private Context context;
    public static RecyclerView.Adapter pixAdapter;

    private OnFragmentInteractionListener mListener;

    public PicturesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param pos DOCUMENT_INDEX.
     * @return A new instance of fragment PicturesFragment.
     */
    public static PicturesFragment newInstance(int pos) {
        PicturesFragment fragment = new PicturesFragment();
        Bundle args = new Bundle();
        args.putInt("DOCUMENT_INDEX", pos);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getArguments();
        if (extras != null) {
            pos = extras.getInt("DOCUMENT_INDEX");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_pictures, container, false);
        context = getActivity();
        try {
            //noinspection ConstantConditions
            ((AppCompatActivity) context)
                    .getSupportActionBar()
                    .setTitle(MainActivity.documentObjectsList.get(pos).getName() + ": Files");
        } catch (Exception e) {
            e.printStackTrace();
        }

        FilesRecyclerAdapter.documentObject = MainActivity.documentObjectsList.get(pos);


        RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.recyclerViewPictures);
        pixAdapter = new FilesRecyclerAdapter(context);

        GridAutofitLayoutManager gridLayoutManager = new GridAutofitLayoutManager(context, 135);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(pixAdapter);

        FloatingActionButton fab = (FloatingActionButton) v.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Camera for add pictures", Snackbar.LENGTH_LONG)
                //.setAction("Action", null).show();

                ((MainActivity) context).TakePicture(pos);

            }
        });

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onBackPressed() {
        Log.e("UserObj", "We pressed back");
        if (MainActivity.adapter != null) {
            MainActivity.adapter.notifyDataSetChanged();
        }

        try {
            //noinspection ConstantConditions
            ((AppCompatActivity)context).getSupportActionBar().setTitle("Pixel");
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
