package com.adeoye.pixel.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.adeoye.pixel.LoginActivity;
import com.adeoye.pixel.MainActivity;
import com.adeoye.pixel.PicturesFragment;
import com.adeoye.pixel.R;
import com.adeoye.pixel.model.DocumentObject;
import com.adeoye.pixel.model.DocumentObject;
import com.adeoye.pixel.model.FileObject;
import com.adeoye.pixel.model.FileObject;
import com.adeoye.pixel.model.UserObject;
import com.adeoye.pixel.util.CountingFileRequestBody;
import com.adeoye.pixel.util.MyDBHandler;
import com.adeoye.pixel.util.Sync;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import okhttp3.Headers;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Created by Akinola on 6/22/2016.
 * This is the custom adapter for the albums
 */
public class DocumentsRecyclerAdapter extends RecyclerView.Adapter<DocumentsRecyclerAdapter.RecyclerViewHolder> {
    private Context context;
    private MyDBHandler myDBHandler;
    private Gson gson;
    private SharedPreferences prefs;
    private UserObject user;
    //private final String UPLOAD_ENDPOINT = "http://mgic-sers.remadelive.com/files/jupload";
    private final String UPLOAD_ENDPOINT = "http://mgic-sers.remadelive.com/api/files";
    //public static List<AlbumObject> MainActivity.documentObjectsList;

    public DocumentsRecyclerAdapter(Context context) {
        this.context = context;
        myDBHandler = MyDBHandler.getInstance(context);
        gson = new Gson();
        prefs = PreferenceManager.getDefaultSharedPreferences(context);

        String USER = prefs.getString("USER", "");
        user = gson.fromJson(USER, UserObject.class);
        if (user == null) {
            user = UserObject.getInstance();
        }
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.album_grid_item, parent, false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(v, context);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        final int pos = position;
        holder.txtAlbumName.setText(MainActivity.documentObjectsList.get(pos).getName());
        //Log.e("SaleSniper","Pictures Name: "+picturesNames.get(pos));
        //Log.e("SaleSniper", "FileObject: " + MainActivity.documentObjectsList.get(pos).getCollections().toString());
        //holder.imgPicture.setText(outlets.get(pos).getMerchantAddress());
        List<FileObject> c = MainActivity.documentObjectsList.get(pos).getCollections();
        int size = c.size();

        ImageView[] dp = {holder.imgCover1, holder.imgCover2, holder.imgCover3};
        Uri uri;
        File f;

        int next = size - 1;
        if (next >= 0) {
            f = new File(c.get(next).getPix());
            if (f.exists()) {
                uri = Uri.fromFile(new File(c.get(next).getPix()));
                Picasso.with(context)
                        .load(uri)
                        .into(dp[0]);
            }
        }

        next -= 1;
        if (next >= 0) {
            f = new File(c.get(next).getPix());
            if (f.exists()) {
                uri = Uri.fromFile(new File(c.get(next).getPix()));
                Picasso.with(context)
                        .load(uri)
                        .into(dp[1]);
            }
        }

        next -= 1;
        if (next >= 0) {
            f = new File(c.get(next).getPix());
            if (f.exists()) {
                uri = Uri.fromFile(new File(c.get(next).getPix()));
                Picasso.with(context)
                        .load(uri)
                        .into(dp[2]);
            }
        }
    }

    @Override
    public int getItemCount() {
        return MainActivity.documentObjectsList.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView txtAlbumName;
        ImageView imgCover1;
        ImageView imgCover2;
        ImageView imgCover3;
        Context mcontext;

        public RecyclerViewHolder(View itemView, Context context) {
            super(itemView);
            this.mcontext = context;

            txtAlbumName = (TextView) itemView.findViewById(R.id.txtAlbumName);
            imgCover1 = (ImageView) itemView.findViewById(R.id.imgCover1);
            imgCover2 = (ImageView) itemView.findViewById(R.id.imgCover2);
            imgCover3 = (ImageView) itemView.findViewById(R.id.imgCover3);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            PicturesFragment picturesFragment = PicturesFragment.newInstance(position);

            ((AppCompatActivity) mcontext).getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.content_main, picturesFragment, "PICTURES")
                    .addToBackStack("PICTURES")
                    .commit();

        }

        @Override
        public boolean onLongClick(View v) {
            int position = getAdapterPosition();

            if (position < 0 || position >= MainActivity.documentObjectsList.size()) {
                return true;
            }


            ShowContextMenu(position);


            return true;
        }

        private void ShowContextMenu(final int position) {
            AlertDialog.Builder contextMenu = new AlertDialog.Builder(mcontext);
            final String[] actions;
            if (MainActivity.documentObjectsList.get(position).getCollections()!=null &&
                    MainActivity.documentObjectsList.get(position).getCollections().size()>0){
                actions = new String[]{"Add Files", "Delete Document", "Details", "Upload Document", "View Files"};
            }else{
                actions = new String[]{"Add Files", "Delete Document", "Details", "View Files"};
            }
            contextMenu.setTitle("Actions")
                    .setCancelable(true)
                    .setItems(actions, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            if (actions[i].equalsIgnoreCase("Add Files")) {
                                ((MainActivity) mcontext).TakePicture(position);
                            } else if (actions[i].equalsIgnoreCase("Delete Document")) {
                                ShowDeleteConfirmationDialog(position);
                            }else if (actions[i].equalsIgnoreCase("Details")) {
                                //Details
                                ShowNewAlbumCreationDialog(position);
                            }else if (actions[i].equalsIgnoreCase("Upload Document")) {
                                //Upload album
                                UploadAlbum(position);
                            }else if (actions[i].equalsIgnoreCase("View Files")) {
                                PicturesFragment picturesFragment = PicturesFragment.newInstance(position);

                                ((AppCompatActivity) mcontext).getSupportFragmentManager()
                                        .beginTransaction()
                                        .add(R.id.content_main, picturesFragment, "FILES")
                                        .addToBackStack("FILES")
                                        .commit();
                            }

                        }
                    })
                    .show();
        }


        private void ShowDeleteConfirmationDialog(final int position){
            View v = View.inflate(mcontext, R.layout.delete_album_confirmation_dialog, null);

            AlertDialog.Builder checkInDialog = new AlertDialog.Builder(mcontext);

            checkInDialog.setView(v);
            checkInDialog.setCancelable(true);
            final AlertDialog alert = checkInDialog.create();
            alert.show();

            Button btnCancel = (Button) v.findViewById(R.id.dialogBtnCancel);
            Button btnDelete = (Button) v.findViewById(R.id.dialogBtnDelete);
            ImageView btnClose = (ImageView) v.findViewById(R.id.btnClose);

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alert.dismiss();
                }
            });

            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alert.dismiss();
                }
            });

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removeAt(position);
                    alert.dismiss();
                }
            });
        }

    }

    public void removeAt(int position) {
        List<FileObject> pics = MainActivity.documentObjectsList.get(position).getCollections();
        for (FileObject p:pics) {
            File f = new File(p.getPix());
            if (f.exists()){
                //noinspection ResultOfMethodCallIgnored
                f.delete();
            }
        }

        boolean t = myDBHandler.DeleteDocument(MainActivity.documentObjectsList.get(position));

        //MainActivity.uploadedAlbumObjectList = myDBHandler.GetUploadedAlbums();
        MainActivity.documentObjectsList = myDBHandler.GetUnUploaded();

        notifyItemRemoved(position);
        notifyItemRangeChanged(position, MainActivity.documentObjectsList.size());

        MainActivity.DocumentsFragment.DoListCheck();
    }

    private void UploadAlbum(int position){
        //DO network upload
        DoUploadData(position);
    }

    private void ShowNewAlbumCreationDialog(final int position) {
        final AlertDialog.Builder addAlbumDialog = new AlertDialog.Builder(context);
        addAlbumDialog.setMessage("Album Details");

        View v = View.inflate(context, R.layout.add_album_dialog, null);
        final EditText editAlbumName = (EditText) v.findViewById(R.id.editAlbumName);
        final EditText editAlbumDescription = (EditText) v.findViewById(R.id.editAlbumDescription);
        final EditText editAlbumTags = (EditText) v.findViewById(R.id.editAlbumTags);
        final Spinner spinnerAlbumGroup = (Spinner) v.findViewById(R.id.spinnerAlbumGroup);
        Button btnCreateAlbum = (Button) v.findViewById(R.id.btnCreateAlbum);
        Button btnCancel = (Button) v.findViewById(R.id.btnCancel);
        btnCreateAlbum.setText("Save");

        editAlbumName.setText(MainActivity.documentObjectsList.get(position).getName());
        editAlbumDescription.setText(MainActivity.documentObjectsList.get(position).getDescription());
        editAlbumTags.setText(MainActivity.documentObjectsList.get(position).getTags());

        String s = MainActivity.documentObjectsList.get(position).getGroup();
        int index = Arrays.asList(context.getResources().getStringArray(R.array.album_groups)).indexOf(s);
        if (index>0){
            spinnerAlbumGroup.setSelection(index);
        }

        addAlbumDialog.setView(v);
        final Dialog dialog = addAlbumDialog.create();

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnCreateAlbum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String albumName = editAlbumName.getText().toString();
                String albumDescription = editAlbumDescription.getText().toString();
                String albumTags = editAlbumTags.getText().toString();
                String albumGroup = spinnerAlbumGroup.getSelectedItem().toString();

                MainActivity.documentObjectsList.get(position).setName(albumName);
                MainActivity.documentObjectsList.get(position).setDescription(albumDescription);
                MainActivity.documentObjectsList.get(position).setGroup(albumGroup);
                MainActivity.documentObjectsList.get(position).setTags(albumTags);



                Log.e("Pixel", "album: " + MainActivity.documentObjectsList.get(position).toString());
                boolean t = myDBHandler.UpdateDocument(MainActivity.documentObjectsList.get(position));
                Log.e("Pixel", "album:update " + t);
                notifyDataSetChanged();
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    TextView txtUploadInformation;
    TextView txtUploadFileName;
    TextView txtUploadFileProgress;
    ProgressBar progressValue;
    int noOfFilesCounter = 0;
    int totalUploadFiles = 0;
    Dialog dialog;

    private void ShowUploadProgressDialog(){
        AlertDialog.Builder progressDialog = new AlertDialog.Builder(context);
        progressDialog.setMessage("Uploading Document...");
        View v = View.inflate(context, R.layout.upload_progress_dialog,null);
        txtUploadInformation = (TextView)v.findViewById(R.id.txtUploadInformation);
        txtUploadFileName = (TextView)v.findViewById(R.id.txtUploadFileName);
        txtUploadFileProgress = (TextView)v.findViewById(R.id.txtUploadFileProgress);
        progressValue= (ProgressBar) v.findViewById(R.id.progressValue);
        progressDialog.setView(v);
        dialog = progressDialog.create();
        dialog.show();


    }

    public void DoUploadData(final int position){

        if (TextUtils.isEmpty(user.getAccessToken())){
            Intent i = new Intent(context, LoginActivity.class);
            context.startActivity(i);
            return;
        }

        ShowUploadProgressDialog();

        AsyncTask.execute(new Runnable() {
            String res = "";
            int resCode = 0;
            DocumentObject d = MainActivity.documentObjectsList.get(position);
            @Override
            public void run() {

                try {
                    //final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");

                    //MediaType ctype = MediaType.parse("application/json; charset=utf-8");
                    MultipartBody.Builder builder = new MultipartBody.Builder();

                    builder.setType(MultipartBody.FORM);

                    List<FileObject> pics = d.getCollections();
                    for (FileObject p :pics) {
                        final File f = new File(p.getPix());
                        if (f.exists()){
                            totalUploadFiles++;
                            //builder.addFormDataPart("files[]",f.getName(), RequestBody.create(MEDIA_TYPE_PNG, f));

                            builder.addPart(
                                    Headers.of("Content-Disposition", "form-data; name=\"files[]\"; filename=\"" + f.getName() + "\""),
                                    new CountingFileRequestBody(f, "image/*", new CountingFileRequestBody.ProgressListener() {
                                        long totalSize = f.length();
                                        @Override
                                        public void transferred(long num) {
                                            final float progress = (Math.round(num*10 / (float) totalSize) * 100)/10;
                                            //uploadData.progressValue = (int) progress;
                                            ((AppCompatActivity)context).runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    txtUploadInformation.setText("Uploading: "+noOfFilesCounter+"/"+totalUploadFiles);
                                                    txtUploadFileName.setText(f.getName());
                                                    txtUploadFileProgress.setText(String.valueOf(progress));
                                                    progressValue.setProgress((int)progress);
                                                }
                                            });
                                        }

                                        @Override
                                        public void finished() {
                                            noOfFilesCounter++;
                                        }


                                    })
                            );
                        }
                    }

                    RequestBody requestBody = builder.build();


                    Request request = new Request.Builder()
                            .addHeader("Authorization","Bearer "+user.getAccessToken())
                            .url(UPLOAD_ENDPOINT)
                            .post(requestBody)
                            .build();

                    OkHttpClient client = new OkHttpClient();
                    Response response = client.newCall(request).execute();

                    resCode = response.code();
                    res = response.body().string();

                } catch (IOException e) {
                    resCode = -1;
                    e.printStackTrace();
                }

                totalUploadFiles = 0;
                noOfFilesCounter = 0;

                ((AppCompatActivity)context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (resCode == 200 || resCode == 201 || resCode == 202){
                            Log.e("Salesniper","pictures upladed");
                            //We set is uploaded to 1
                            d.setUploadStatus(true);
                            for (FileObject p:d.getCollections()) {
                                p.setUploadStatus(true);
                            }

                            myDBHandler.UpdateDocument(d);
                            MainActivity.documentObjectsList = myDBHandler.GetUnUploaded();
                            //MainActivity.uploadedAlbumObjectList = myDBHandler.GetUploadedAlbums();

                            notifyItemRemoved(position);
                            notifyItemRangeChanged(position, MainActivity.documentObjectsList.size());

                            MainActivity.DocumentsFragment.DoListCheck();

                        }
                        else if (resCode==-1){
                            // TODO: 9/11/2016
                            // we notify him that the job has been stacked due to time out
                            Log.e("Pixel","Timeout");
                        }else{
                            // TODO: 9/11/2016
                            // we notify him that the job has been stacked
                            Log.e("Pixel","Error");
                        }

                        Sync sync = new Sync(context);
                        sync.GetUploadedFiles();
                        dialog.dismiss();
                    }
                });

                Log.e("Salesniper","Okhttp3: "+res);
            }
        });

    }
}

