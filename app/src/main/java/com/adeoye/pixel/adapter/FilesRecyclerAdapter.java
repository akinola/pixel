package com.adeoye.pixel.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.adeoye.pixel.MainActivity;
import com.adeoye.pixel.R;
import com.adeoye.pixel.model.DocumentObject;
import com.adeoye.pixel.util.MyDBHandler;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.File;

/**
 * Created by Akinola on 10/1/2016.
 *
 */
public class FilesRecyclerAdapter extends RecyclerView.Adapter<FilesRecyclerAdapter.RecyclerViewHolder> {
    private Context context;
    private MyDBHandler myDBHandler;
    public static DocumentObject documentObject;

    public FilesRecyclerAdapter(Context paramContext) {
        context = paramContext;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.picture_grid_item, parent, false);
        return new RecyclerViewHolder(v, context);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        File f = new File(documentObject.getCollections().get(position).getPix());
        if (f.exists()) {
            Uri uri = Uri.fromFile(f);
            Picasso.with(this.context)
                    .load(uri)
                    .into(holder.imgPicture);
        }


        
    }

    @Override
    public int getItemCount() {
        return documentObject.getCollections().size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, View.OnLongClickListener {
        ImageView imgPicture;
        Context mcontext;

        public RecyclerViewHolder(View itemView, Context context) {
            super(itemView);
            this.mcontext = context;
            imgPicture = (ImageView) itemView.findViewById(R.id.imgPicture);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public void onClick(View paramView) {
            int i = getAdapterPosition();
            Log.e("hshas", "We clicked: " + documentObject.getCollections().get(i).getPix());
            ShowFullImageDialog(i);
        }

        public boolean onLongClick(View paramView) {
            int i = getAdapterPosition();
            ShowContextMenu(i);
            return true;
        }

        private void ShowContextMenu(final int pos) {

        }


        int currentImage;
        ImageSwitcherPicasso mImageSwitcherPicasso;
        Dialog dialog;

        private void ShowFullImageDialog(int index) {
            currentImage = index;
            final AlertDialog.Builder sdialog = new AlertDialog.Builder(context);
            //dialog = new Dialog(context);
            // Set the title
            //dialog.setTitle(documentObject.getCollections().get(currentImage).getPix());
            //sdialog.setMessage(documentObject.getCollections().get(currentImage).getPix());

            View v = View.inflate(context, R.layout.pictures_preview, null);
            sdialog.setView(v);

            final TextView txtPictureName = (TextView)v.findViewById(R.id.txtPictureName);
            txtPictureName.setText(documentObject.getCollections().get(currentImage).getPix());
            ImageView prev = (ImageView) v.findViewById(R.id.btnPrevious);
            ImageView next = (ImageView) v.findViewById(R.id.btnNext);

            final ImageSwitcher switcher = (ImageSwitcher) v.findViewById(R.id.imageSwitcher);
            switcher.setFactory(new ViewSwitcher.ViewFactory() {
                @Override
                public View makeView() {
                    ImageView myView = new ImageView(context);
                    myView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    myView.setLayoutParams(new ImageSwitcher.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

                    return myView;
                }
            });

            mImageSwitcherPicasso = new ImageSwitcherPicasso(context, switcher);

            if (currentImage >= 0 && currentImage < getItemCount()) {
                File f = new File(documentObject.getCollections().get(currentImage).getPix());
                if (f.exists()) {
                    Log.e("hshas", "file existssds");
                    Uri uri = Uri.fromFile(f);
                    txtPictureName.setText(f.getName());
                    Picasso.with(context)
                            .load(uri)
                            .placeholder(R.drawable.placeholder)
                            .error(R.drawable.no_image_available)
                            .into(mImageSwitcherPicasso);
                }

            }

            prev.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //switcher.setImageResource(R.drawable.document);
                    if (currentImage > 0 ) {
                        currentImage--;
                        File f = new File(documentObject.getCollections().get(currentImage).getPix());
                        if (f.exists()) {
                            Uri uri = Uri.fromFile(f);
                            txtPictureName.setText(f.getName());
                            Picasso.with(context)
                                    .load(uri)
                                    .placeholder(R.drawable.placeholder)
                                    .error(R.drawable.no_image_available)
                                    .into(mImageSwitcherPicasso);
                        }

                    }
                }
            });

            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //switcher.setImageResource(R.drawable.help4);

                    if (currentImage < getItemCount() - 1) {
                        currentImage++;
                        File f = new File(documentObject.getCollections().get(currentImage).getPix());
                        if (f.exists()) {
                            Uri uri = Uri.fromFile(f);
                            txtPictureName.setText(f.getName());
                            Picasso.with(context)
                                    .load(uri)
                                    .placeholder(R.drawable.placeholder)
                                    .error(R.drawable.no_image_available)
                                    .networkPolicy(NetworkPolicy.OFFLINE)
                                    .into(mImageSwitcherPicasso);
                        }
                    }
                }
            });

            dialog=sdialog.create();
            // Display the dialog
            dialog.show();
        }
    }
}
