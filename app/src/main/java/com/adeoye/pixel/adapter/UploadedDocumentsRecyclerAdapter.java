package com.adeoye.pixel.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.adeoye.pixel.MainActivity;
import com.adeoye.pixel.R;
import com.adeoye.pixel.model.UserObject;
import com.adeoye.pixel.util.ProgressResponseBody;
import com.adeoye.pixel.util.ServiceHandler;
import com.google.gson.Gson;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Akinola on 10/1/2016.
 */
public class UploadedDocumentsRecyclerAdapter extends RecyclerView.Adapter<UploadedDocumentsRecyclerAdapter.RecyclerViewHolder> {
    private final String FILES_URL = "http://mgic-sers.remadelive.com/api/files/";
    private Context context;
    //private Gson gson;
    public int imgCounter;
    //private MyDBHandler myDBHandler;
    //private SharedPreferences prefs;
    private UserObject user;
    OkHttpClient client;
    int SIZE_OF_CACHE = 10000000;
    Picasso picasso;
    String root;
    File myDir;

    public UploadedDocumentsRecyclerAdapter(Context paramContext) {
        context = paramContext;
        Gson gson = new Gson();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        String USER = prefs.getString("USER", "");
        user = gson.fromJson(USER, UserObject.class);
        if (user == null) {
            user = UserObject.getInstance();
        }

        root = Environment.getExternalStorageDirectory().toString();
        myDir = new File(root + "/Pixel");

        if (!myDir.exists() || !myDir.isDirectory()) {
            //noinspection ResultOfMethodCallIgnored
            myDir.mkdirs();
            Log.e("ASYNC", "We created the tirlal  direnctry");
        }




        File customCacheDirectory = new File(Environment.getExternalStorageDirectory().getAbsoluteFile() + "/MyCache");
        Cache cache = new Cache(customCacheDirectory, Integer.MAX_VALUE);
        client = new OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request().newBuilder()
                                .addHeader("Authorization", "Bearer " + user.getAccessToken())
                                /*.cacheControl(new CacheControl.Builder()
                                        .onlyIfCached()
                                        .build())*/
                                .build();
                        return chain.proceed(request);
                        /*Request request = chain.request();

                        if (isOnline()) {
                            request = request.newBuilder()
                                    .addHeader("Authorization", "Bearer " + user.getAccessToken())
                                    .header("Cache-Control", "public, max-age=" + 60)
                                    .build();
                        } else {
                            request = request.newBuilder()
                                    .addHeader("Authorization", "Bearer " + user.getAccessToken())
                                    .header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7)
                                    .build();
                        }

                        */
                    }
                }).addNetworkInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        try {
                            Response originalResponse = chain.proceed(chain.request());
                            return originalResponse.newBuilder()
                                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                                    .build();
                        } catch (SocketTimeoutException e) {
                            //e.printStackTrace();
                            if (progressListener != null) {
                                progressListener.onConnectionTimeout();
                            }
                        }
                        return chain.proceed(chain.request());
                    }
                }).build();


    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.picture_grid_item, parent, false);
        return new RecyclerViewHolder(v, context);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        File f = new File (myDir.getAbsolutePath()+"/"+MainActivity.uploadedFileObjects.get(position).getName());
        if (!f.exists()){
            GetBitmapFromNet getBitmapFromNet = new GetBitmapFromNet(MainActivity.uploadedFileObjects.get(position).getName(),
                    FILES_URL + MainActivity.uploadedFileObjects.get(position).getId());
            getBitmapFromNet.execute((Void)null);

            picasso = new Picasso.Builder(context).
                    downloader(new OkHttp3Downloader(client))
                    .build();

            picasso.load(FILES_URL + MainActivity.uploadedFileObjects.get(position).getId())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.no_image_available)
                    //.networkPolicy(NetworkPolicy.OFFLINE)
                    .fit()
                    .centerInside()
                    .into(holder.imgPicture);
        }else {

            Picasso.with(context)
                    .load(f)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.no_image_available)
                    .fit()
                    .centerInside()
                    .into(holder.imgPicture);
        }

    }

    final ProgressResponseBody.ProgressListener progressListener = new ProgressResponseBody.ProgressListener() {
        @Override
        public void update(long bytesRead, long contentLength, boolean done) {
            float percent = (float) bytesRead / (float) contentLength;
            //System.out.format("%d%% done\n", (100 * bytesRead) / contentLength);
            if (percent >= 0.8) {
                imgCounter++;
                Log.e("Pixel", "Downloaded " + imgCounter);
            }
        }

        @Override
        public void onConnectionTimeout() {
            imgCounter++;
            Log.e("Pixel", "Failed " + imgCounter);
        }
    };

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


    @Override
    public int getItemCount() {
        return MainActivity.uploadedFileObjects.size();
    }

    public class GetBitmapFromNet extends AsyncTask<Void, String, Boolean>
    {
        String fileName;
        String url;
        ServiceHandler serviceHandler;
        public GetBitmapFromNet(String fileName,String url) {
            serviceHandler = new ServiceHandler(context);
            this.fileName = fileName;
            this.url = url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            serviceHandler.DownloadFile(fileName,url);
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
        }
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, View.OnLongClickListener {
        ImageView imgPicture;
        Context mcontext;

        public RecyclerViewHolder(View itemView, Context context) {
            super(itemView);
            this.mcontext = context;
            imgPicture = (ImageView) itemView.findViewById(R.id.imgPicture);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public void onClick(View paramView) {
            int i = getAdapterPosition();
            //Log.e("hshas", "We clicked: " + MainActivity.uploadedFileObjects.get(i).getId() + ": " + MainActivity.uploadedFileObjects.get(i).getName());
            ShowFullImageDialog(i);
        }

        public boolean onLongClick(View paramView) {
            int i = getAdapterPosition();
            ShowContextMenu(i);
            return true;
        }

        private void ShowContextMenu(final int pos) {

        }



        int currentImage;
        ImageSwitcherPicasso mImageSwitcherPicasso;
        Dialog dialog;
        File f;
        private void ShowFullImageDialog(int index) {
            currentImage = index;



            final AlertDialog.Builder sdialog = new AlertDialog.Builder(context);
            //dialog = new Dialog(context);
            // Set the title

            View v = View.inflate(context, R.layout.pictures_preview, null);
            sdialog.setView(v);

            final TextView txtPictureName = (TextView)v.findViewById(R.id.txtPictureName);
            txtPictureName.setText(MainActivity.uploadedFileObjects.get(index).getName());
            ImageView prev = (ImageView) v.findViewById(R.id.btnPrevious);
            ImageView next = (ImageView) v.findViewById(R.id.btnNext);

            final ImageSwitcher switcher = (ImageSwitcher) v.findViewById(R.id.imageSwitcher);
            switcher.setFactory(new ViewSwitcher.ViewFactory() {
                @Override
                public View makeView() {
                    ImageView myView = new ImageView(context);
                    myView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    myView.setLayoutParams(new ImageSwitcher.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

                    return myView;
                }
            });

            mImageSwitcherPicasso = new ImageSwitcherPicasso(context, switcher);

            if (currentImage >= 0 && currentImage < getItemCount()) {

                txtPictureName.setText(MainActivity.uploadedFileObjects.get(currentImage).getName());
                f = new File (myDir.getAbsolutePath()+"/"+MainActivity.uploadedFileObjects.get(currentImage).getName());
                if (!f.exists()) {
                    GetBitmapFromNet getBitmapFromNet = new GetBitmapFromNet(MainActivity.uploadedFileObjects.get(currentImage).getName(),
                            FILES_URL + MainActivity.uploadedFileObjects.get(currentImage).getId());
                    getBitmapFromNet.execute((Void) null);

                    picasso = new Picasso.Builder(context).
                            downloader(new OkHttp3Downloader(client))
                            .build();

                    Log.e("hshas", "We clicked: " +FILES_URL + MainActivity.uploadedFileObjects.get(currentImage).getId());
                    picasso.load(FILES_URL + MainActivity.uploadedFileObjects.get(currentImage).getId())
                            .placeholder(R.drawable.placeholder)
                            .error(R.drawable.no_image_available)
                            .into(mImageSwitcherPicasso);
                }else{
                    Log.e("hshas", "we obtain from storage: " +FILES_URL + MainActivity.uploadedFileObjects.get(currentImage).getId());
                    Picasso.with(context)
                            .load(f)
                            .placeholder(R.drawable.placeholder)
                            .error(R.drawable.no_image_available)
                            .into(mImageSwitcherPicasso);
                }


            }

            prev.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //switcher.setImageResource(R.drawable.document);
                    if (currentImage>0) {
                        currentImage--;
                        f = new File (myDir.getAbsolutePath()+"/"+MainActivity.uploadedFileObjects.get(currentImage).getName());
                        if (!f.exists()) {
                            GetBitmapFromNet getBitmapFromNet = new GetBitmapFromNet(MainActivity.uploadedFileObjects.get(currentImage).getName(),
                                    FILES_URL + MainActivity.uploadedFileObjects.get(currentImage).getId());
                            getBitmapFromNet.execute((Void) null);

                            picasso = new Picasso.Builder(context).
                                    downloader(new OkHttp3Downloader(client))
                                    .build();
                            Log.e("hshas", "we download from net: " +FILES_URL + MainActivity.uploadedFileObjects.get(currentImage).getId());
                            picasso.load(FILES_URL + MainActivity.uploadedFileObjects.get(currentImage).getId())
                                    .placeholder(R.drawable.placeholder)
                                    .error(R.drawable.no_image_available)
                                    .into(mImageSwitcherPicasso);
                        }else{
                            Log.e("hshas", "we obtain from storage: " +FILES_URL + MainActivity.uploadedFileObjects.get(currentImage).getId());
                            Picasso.with(context)
                                    .load(f)
                                    .placeholder(R.drawable.placeholder)
                                    .error(R.drawable.no_image_available)
                                    .into(mImageSwitcherPicasso);
                        }

                        txtPictureName.setText(MainActivity.uploadedFileObjects.get(currentImage).getName());

                    }
                }
            });

            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //switcher.setImageResource(R.drawable.help4);

                    if (currentImage<getItemCount()-1) {
                        currentImage++;
                        f = new File (myDir.getAbsolutePath()+"/"+MainActivity.uploadedFileObjects.get(currentImage).getName());
                        if (!f.exists()) {
                            GetBitmapFromNet getBitmapFromNet = new GetBitmapFromNet(MainActivity.uploadedFileObjects.get(currentImage).getName(),
                                    FILES_URL + MainActivity.uploadedFileObjects.get(currentImage).getId());
                            getBitmapFromNet.execute((Void) null);

                            picasso = new Picasso.Builder(context).
                                    downloader(new OkHttp3Downloader(client))
                                    .build();
                            Log.e("hshas", "we download from net: " +FILES_URL + MainActivity.uploadedFileObjects.get(currentImage).getId());
                            picasso.load(FILES_URL + MainActivity.uploadedFileObjects.get(currentImage).getId())
                                    .placeholder(R.drawable.placeholder)
                                    .error(R.drawable.no_image_available)
                                    .into(mImageSwitcherPicasso);
                        }else{
                            Log.e("hshas", "we obtain from storage: " +FILES_URL + MainActivity.uploadedFileObjects.get(currentImage).getId());
                            Picasso.with(context)
                                    .load(f)
                                    .placeholder(R.drawable.placeholder)
                                    .error(R.drawable.no_image_available)
                                    .into(mImageSwitcherPicasso);
                        }
                        txtPictureName.setText(MainActivity.uploadedFileObjects.get(currentImage).getName());
                    }
                }
            });

            dialog = sdialog.create();
            // Display the dialog
            dialog.show();
        }
    }
}
