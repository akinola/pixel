package com.adeoye.pixel.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 10/1/2016.
 */
public class DocumentObject implements Parcelable {

    private List<FileObject> collections;
    private String description = "";
    private String group = "";
    private int id;
    private String lastModified = "";
    private String name = "";
    private String tags = "";
    private boolean uploadStatus;


    public DocumentObject() {
        collections = new ArrayList<>();
    }

    public List<FileObject> getCollections() {
        return collections;
    }

    public void setCollections(List<FileObject> collections) {
        this.collections = collections;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public boolean isUploadStatus() {
        return uploadStatus;
    }

    public void setUploadStatus(boolean uploadStatus) {
        this.uploadStatus = uploadStatus;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.collections);
        dest.writeString(this.description);
        dest.writeString(this.group);
        dest.writeInt(this.id);
        dest.writeString(this.lastModified);
        dest.writeString(this.name);
        dest.writeString(this.tags);
        dest.writeByte(this.uploadStatus ? (byte) 1 : (byte) 0);
    }

    protected DocumentObject(Parcel in) {
        this.collections = in.createTypedArrayList(FileObject.CREATOR);
        this.description = in.readString();
        this.group = in.readString();
        this.id = in.readInt();
        this.lastModified = in.readString();
        this.name = in.readString();
        this.tags = in.readString();
        this.uploadStatus = in.readByte() != 0;
    }

    public static final Parcelable.Creator<DocumentObject> CREATOR = new Parcelable.Creator<DocumentObject>() {
        @Override
        public DocumentObject createFromParcel(Parcel source) {
            return new DocumentObject(source);
        }

        @Override
        public DocumentObject[] newArray(int size) {
            return new DocumentObject[size];
        }
    };
}
