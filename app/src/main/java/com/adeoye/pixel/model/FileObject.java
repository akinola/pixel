package com.adeoye.pixel.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by User on 10/1/2016.
 */
public class FileObject implements Parcelable {
    private int id;
    private String pix = "";
    private boolean uploadStatus;

    public FileObject() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPix() {
        return pix;
    }

    public void setPix(String pix) {
        this.pix = pix;
    }

    public boolean isUploadStatus() {
        return uploadStatus;
    }

    public void setUploadStatus(boolean uploadStatus) {
        this.uploadStatus = uploadStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.pix);
        dest.writeByte(this.uploadStatus ? (byte) 1 : (byte) 0);
    }

    protected FileObject(Parcel in) {
        this.id = in.readInt();
        this.pix = in.readString();
        this.uploadStatus = in.readByte() != 0;
    }

    public static final Parcelable.Creator<FileObject> CREATOR = new Parcelable.Creator<FileObject>() {
        @Override
        public FileObject createFromParcel(Parcel source) {
            return new FileObject(source);
        }

        @Override
        public FileObject[] newArray(int size) {
            return new FileObject[size];
        }
    };
}
