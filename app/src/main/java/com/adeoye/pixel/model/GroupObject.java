package com.adeoye.pixel.model;

/**
 * Created by User on 10/1/2016.
 */
public class GroupObject {

    private String description;
    private int id;
    private String name;
    private String slug;

    public GroupObject() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}
