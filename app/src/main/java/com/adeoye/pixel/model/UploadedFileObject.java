package com.adeoye.pixel.model;

/**
 * Created by User on 10/1/2016.
 */
public class UploadedFileObject {
    private int id;
    private String name;
    private String store_path;

    public UploadedFileObject() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStore_path() {
        return store_path;
    }

    public void setStore_path(String store_path) {
        this.store_path = store_path;
    }
}
