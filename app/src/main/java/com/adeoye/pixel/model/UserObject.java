package com.adeoye.pixel.model;

/**
 * Created by User on 10/1/2016.
 */
public class UserObject {

    private static UserObject userObject;
    private String accessToken = "";
    private String coverPixUrl = "";
    private String displayPixUrl = "";
    private String email = "";
    private String firstName = "";
    private int id;
    private String lastName = "";
    private String password = "";
    private String phoneNumber = "";
    private String refreshToken = "";

    private UserObject() {
    }

    public static UserObject getInstance(){
        if (userObject==null){
            userObject = new UserObject();
        }
        return userObject;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getCoverPixUrl() {
        return coverPixUrl;
    }

    public void setCoverPixUrl(String coverPixUrl) {
        this.coverPixUrl = coverPixUrl;
    }

    public String getDisplayPixUrl() {
        return displayPixUrl;
    }

    public void setDisplayPixUrl(String displayPixUrl) {
        this.displayPixUrl = displayPixUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
