package com.adeoye.pixel.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.adeoye.pixel.model.DocumentObject;
import com.adeoye.pixel.model.FileObject;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class MyDBHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static String DB_PATH = "/data/data/com.adeoye.pixel/databases/";
    private static final String DATABASE_NAME = "pixel.sqlite";

    private static MyDBHandler myDBHandler;

    //Tables
    private final String DOCUMENTS_TABLE = "documents";

    //Fields
    private final String COLLECTIONS = "collections";
    private final String DESCRIPTION = "description";
    private final String GROUP = "groop";  //this is deliberate
    private final String ID = "_id";
    private final String LAST_MODIFIED = "last_modified";
    private final String NAME = "name";
    private final String TAGS = "tags";
    private final String UPLOAD_STATUS = "upload_status";


    private SQLiteDatabase db;
    private Gson gson;

    private final Context myContext;


    /*public MyDBHandler(Context context, String name, CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
        this.myContext = context;
    }*/

    public MyDBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.myContext = context;
        gson = new Gson();
        //mDatabaseFile = context.getDatabasePath(DATABASE_NAME);
    }

    public void createDataBase() throws IOException {

        //Log.e("MyJob","Let us check if the Database exists onCreate");
        boolean dbExist = checkDataBase();
        if (dbExist) {
            //database already exists
            //Log.e("MyJob","Database exists onCreate");
        } else {
            //Log.e("MyJob","Database doesn't exists onCreate so we copy the file");
            try {
                this.getReadableDatabase();
            } catch (Exception ex) {
                //Log.e("MyJob","Failed at 1");
            }

            try {
                copyDataBase();
                //this.close(); //under check 17-09-2015
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    private boolean checkDataBase() {
        SQLiteDatabase checkDB = null;
        try {
            String myPath = DB_PATH + DATABASE_NAME;
            //Log.e("MyJob","Lets check if the db file exists by opening it");
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
            //Log.e("MyJob","The file exists so we open");
        } catch (SQLiteException e) {
            //Log.e("MyJob","The file doesn't exist onCheck");
            if (checkDB != null) {
                checkDB.close();
            }
        }
        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB != null ? true : false;
    }

    private void copyDataBase() throws IOException {
        InputStream myInput = myContext.getAssets().open(DATABASE_NAME);
        String outFileName = DB_PATH + DATABASE_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }


    public void openDataBase() throws SQLException {
        String myPath = DB_PATH + DATABASE_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
        if (db != null) {
            db.close();
        }
    }

    @Override
    public synchronized void close() {
        //super.close();
        if (db != null && db.isOpen()) {
            db.close();
        }

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = String.format(Locale.ENGLISH, "Create table if not exists %s " +
                "(%s integer primary key," +
                "%s text," +
                "%s text," +
                "%s text," +
                "%s text," +
                "%s text," +
                "%s integer DEFAULT 0," +
                "%s text)", DOCUMENTS_TABLE, ID, NAME, DESCRIPTION, TAGS, GROUP, LAST_MODIFIED, UPLOAD_STATUS, COLLECTIONS);

        //String query = "create table if not exists albums (_id integer primary key, name text,description text,tags text,groop text,last_modified text,upload_status integer DEFAULT 0,collections text)"
        db.execSQL(query);
    }

    public static synchronized MyDBHandler getInstance(Context context) {
        if (myDBHandler == null) {
            myDBHandler = new MyDBHandler(context.getApplicationContext());
        }
        return myDBHandler;
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + DOCUMENTS_TABLE);

        // Create tables again
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        this.onUpgrade(db, oldVersion, newVersion);
    }


    public List<DocumentObject> GetUnUploaded() {
        List<DocumentObject> documentObjectList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format(Locale.ENGLISH, "SELECT * FROM %s WHERE %s = 0", DOCUMENTS_TABLE, UPLOAD_STATUS);
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                DocumentObject d = new DocumentObject();
                d.setId(cursor.getInt(0));
                d.setName(cursor.getString(1));
                d.setDescription(cursor.getString(2));
                d.setTags(cursor.getString(3));
                d.setGroup(cursor.getString(4));
                d.setLastModified(cursor.getString(5));

                String files = cursor.getString(7);
                Type listType = new TypeToken<List<FileObject>>() {
                }.getType();
                List<FileObject> f = new Gson().fromJson(files, listType);
                if (f == null) {
                    f = new ArrayList<>();
                }

                List<FileObject> f2 = new ArrayList<>();
                for (FileObject f1 : f) {
                    if (new File(f1.getPix()).exists()) {
                        f2.add(f1);
                    }
                }


                d.setCollections(f2);
                documentObjectList.add(d);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return documentObjectList;
    }

    public boolean UpdateDocument(DocumentObject d) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(NAME, d.getName());
        values.put(DESCRIPTION, d.getDescription());
        values.put(TAGS, d.getTags());
        values.put(GROUP, d.getGroup());
        values.put(LAST_MODIFIED, d.getLastModified());
        values.put(UPLOAD_STATUS, d.isUploadStatus() ? 1 : 0);
        values.put(COLLECTIONS, this.gson.toJson(d.getCollections()));
        try {
            db.update(DOCUMENTS_TABLE, values, ID + " = " + d.getId(), null);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public boolean DeleteDocument(DocumentObject d) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = String.format(Locale.ENGLISH, "DELETE  FROM %s WHERE %s = %d", DOCUMENTS_TABLE, ID, d.getId());

        try {
            db.execSQL(query);
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public boolean InsertDocument(DocumentObject d) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(NAME, d.getName());
        values.put(DESCRIPTION, d.getDescription());
        values.put(TAGS, d.getTags());
        values.put(GROUP, d.getGroup());
        values.put(LAST_MODIFIED, d.getLastModified());
        values.put(UPLOAD_STATUS, d.isUploadStatus() ? 1 : 0);
        values.put(COLLECTIONS, this.gson.toJson(d.getCollections()));
        try {
            db.insert(DOCUMENTS_TABLE, null, values);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }


}











