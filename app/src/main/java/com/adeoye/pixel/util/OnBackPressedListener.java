package com.adeoye.pixel.util;

public abstract interface OnBackPressedListener{
  public abstract void onBackPressed();
}
