package com.adeoye.pixel.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;

import com.adeoye.pixel.model.UserObject;
import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Akinola on 10/4/2016.
 *
 */
public class ServiceHandler {

    private UserObject user;
    String root;
    File myDir;
    public ServiceHandler(Context context) {
        Gson gson = new Gson();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String USER = prefs.getString("USER", "");
        user = gson.fromJson(USER, UserObject.class);
        if (user == null) {
            user = UserObject.getInstance();
        }

        root = Environment.getExternalStorageDirectory().toString();
        myDir = new File(root + "/Pixel");

        if (!myDir.exists() || !myDir.isDirectory()) {
            //noinspection ResultOfMethodCallIgnored
            myDir.mkdirs();
            Log.e("ASYNC", "We created the tirlal  direnctry");
        }
    }

    public void DownloadFile(String fileName,String url){

        try   {
            URL metaUrl=new URL(url);
            HttpURLConnection con = (HttpURLConnection) metaUrl.openConnection();
            con.addRequestProperty("Authorization", "Bearer " + user.getAccessToken());
            con.connect();

            InputStream is = new BufferedInputStream(con.getInputStream(),1024);
            File dataFile=new File(myDir.getAbsolutePath()+"/"+fileName);

            FileOutputStream outputStream=new FileOutputStream(dataFile);

            byte buffer[]=new byte[1024];
            int dataSize;
            //int loadedSize=0;

            while((dataSize=is.read(buffer))!=-1) {
                //loadedSize+=dataSize;
                outputStream.write(buffer,0,dataSize);
            }
            Log.e("ASYNC", "downloaded to: " + dataFile.getAbsolutePath());
        }
        catch (Exception e) {
            //Log.w("ASYNC", "Streaming "+fileN+" from server failed; using default image");
            e.printStackTrace();
        }
    }
}
