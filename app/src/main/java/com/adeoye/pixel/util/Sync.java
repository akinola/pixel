package com.adeoye.pixel.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;

import com.adeoye.pixel.LoginActivity;
import com.adeoye.pixel.MainActivity;
import com.adeoye.pixel.R;
import com.adeoye.pixel.adapter.UploadedDocumentsRecyclerAdapter;
import com.adeoye.pixel.model.DocumentObject;
import com.adeoye.pixel.model.GroupObject;
import com.adeoye.pixel.model.LoginGoodResponse;
import com.adeoye.pixel.model.FileObject;
import com.adeoye.pixel.model.UploadedFileObject;
import com.adeoye.pixel.model.UserObject;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by AKinola on 9/2/2016.
 *
 */
public class Sync {

    private Context context;

    private final String UPLOAD_ENDPOINT = "http://mgic-sers.remadelive.com/files/jupload";
    private final String LOGIN_ENDPOINT = "http://mgic-sers.remadelive.com/oauth/token";
    private final String GROUPS_ENDPOINT = "http://mgic-sers.remadelive.com/api/document-groups";
    private final String GET_USER_UPLOADED_FILES = "http://mgic-sers.remadelive.com/api/files";
    private final String ERROR_CONNECTION_MESSAGE = "The Server couldn't be reached\nPlease ensure you are connected to the internet";
    private final String SERVER_ERROR_CONNECTION_MESSAGE = "The Server couldn't authenticate the user.";


    private MyDBHandler myDBHandler;
    private SharedPreferences prefs;
    private Gson gson;
    //private String ACCESS_TOKEN="";
    private OkHttpClient client;
    private UserObject user;


    public Sync(Context context) {
        this.context = context;
        myDBHandler = MyDBHandler.getInstance(context);
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        gson = new Gson();
        String USER = prefs.getString("USER", "");
        user = gson.fromJson(USER, UserObject.class);
        if (user == null) {
            user = UserObject.getInstance();
        }
    }

    public void DoLogin(final String username, final String password){

        AsyncTask.execute(new Runnable() {
            String res = "";
            int resCode = 0;
            @Override
            public void run() {
                try {
                    RequestBody formBody = new FormBody.Builder()
                            .add("username", username)
                            .add("password", password)
                            .add("client_id", "2")
                            .add("grant_type", "password")
                            .add("client_secret", "K9ShWBYJvWvOL57PfX6Q6PJQr0vr8aAmTz7RlHDK")
                            .build();

                    Request request = new Request.Builder()
                            .post(formBody)
                            .url(LOGIN_ENDPOINT)
                            .build();

                    client = new OkHttpClient.Builder()
                            .connectTimeout(15, TimeUnit.SECONDS)
                            .writeTimeout(15, TimeUnit.SECONDS)
                            .readTimeout(15, TimeUnit.SECONDS)
                            .build();

                    Response response = client.newCall(request).execute();

                    resCode = response.code();

                    res = response.body().string();

                } catch (IOException e) {
                    resCode = -1;
                    e.printStackTrace();
                }

                if (resCode == 200 || resCode == 201 || resCode == 202){
                    LoginGoodResponse goodResponse = gson.fromJson(res,LoginGoodResponse.class);
                    user.setAccessToken(goodResponse.getAccess_token());
                    user.setRefreshToken(goodResponse.getRefresh_token());

                    String USER = gson.toJson(user);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("USER", USER);
                    editor.apply();

                    GetGroups();
                    GetUploadedFiles();

                    Intent i = new Intent(context, MainActivity.class);
                    context.startActivity(i);
                    ((AppCompatActivity)context).finish();

                }
                else if (resCode==-1){
                    // TODO: 9/11/2016
                    // we notify him that the job has been stacked due to time out
                    ((AppCompatActivity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ShowAlert(ERROR_CONNECTION_MESSAGE, "Error");
                        }
                    });
                }else{
                    // TODO: 9/11/2016
                    // we notify him that the job has been stacked
                    ((AppCompatActivity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ShowAlert(SERVER_ERROR_CONNECTION_MESSAGE, "Error");
                        }
                    });
                }
            }

        });
    }

    public void ShowAlert(String message, String title) {
        try {
            AlertDialog.Builder alert = new AlertDialog.Builder(context);
            alert.setTitle(title);
            alert.setMessage(message);
            alert.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (isOnline()) {
                        DoLogin(user.getEmail(), user.getPassword());
                    } else {
                        ShowAlert(ERROR_CONNECTION_MESSAGE, "Error");
                    }
                }
            }).setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    ((AppCompatActivity)context).finish();
                }
            }).setCancelable(false);
            alert.setIcon(R.drawable.ic_error_black);
            alert.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public void GetGroups(){
        AsyncTask.execute(new Runnable() {
            String res = "";
            int resCode = 0;

            @Override
            public void run() {
                try {
                    Request request = new Request.Builder()
                            .addHeader("Authorization","Bearer "+user.getAccessToken())
                            .url(GROUPS_ENDPOINT)
                            .build();

                    Response response = client.newCall(request).execute();

                    resCode = response.code();
                    Log.e("Pixel","Groups retreival "+resCode);
                    res =  response.body().string();
                } catch (Exception ex) {
                    resCode = -1;
                    ex.printStackTrace();
                }

                if (resCode == 200 || resCode == 201 || resCode == 202){
                    Type listType = new TypeToken<List<GroupObject>>() {}.getType();
                    List<GroupObject> g = new Gson().fromJson(res, listType);
                    //List<GroupObject> g = gson.fromJson(res,GroupObject.class);
                    if (g!=null){
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("GROUPS", res);
                        editor.apply();
                    }

                } else if (resCode==-1){
                    Log.e("Pixel","Groups retrieval Timeout");
                }else{
                    Log.e("Pixel","Groups retrieval Error: "+resCode);
                }
            }
        });
    }

    public void GetUploadedFiles(){
        if (MainActivity.uploadedFileObjects==null){
            MainActivity.uploadedFileObjects=new ArrayList<>();
        }

        AsyncTask.execute(new Runnable() {
            String res = "";
            int resCode = 0;

            @Override
            public void run() {
                try {
                    Request request = new Request.Builder()
                            .addHeader("Authorization","Bearer "+user.getAccessToken())
                            .url(GET_USER_UPLOADED_FILES)
                            .build();

                    Response response = client.newCall(request).execute();

                    resCode = response.code();
                    Log.e("Pixel","files retrieval "+resCode);
                    res =  response.body().string();
                } catch (Exception ex) {
                    resCode = -1;
                    ex.printStackTrace();
                }

                if (resCode == 200 || resCode == 201 || resCode == 202){
                    try {
                        Type listType = new TypeToken<List<UploadedFileObject>>() {}.getType();
                        MainActivity.uploadedFileObjects = new Gson().fromJson(res, listType);
                        Log.e("Pixel","files retrieval: "+res);
                        if (MainActivity.uploadedFileObjects==null){
                            MainActivity.uploadedFileObjects=new ArrayList<>();
                            Log.e("Pixel","files retrieval: null ");
                        }

                        Log.e("Pixel","files retrieval: "+MainActivity.uploadedFileObjects.size());

                        if (MainActivity.adapterUploaded==null) {
                            MainActivity.adapterUploaded = new UploadedDocumentsRecyclerAdapter(context);
                        }
                        ((AppCompatActivity)context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                MainActivity.adapterUploaded.notifyDataSetChanged();
                            }
                        });
                    } catch (JsonSyntaxException e) {
                        e.printStackTrace();
                        Log.e("Pixel","Uploaded but server returned an error");
                    }


                } else if (resCode==-1){
                    Log.e("Pixel","Files retrievel Timeout");
                }else{
                    Log.e("Pixel","Files  retrieval Error: "+resCode);
                }
            }
        });
    }

    public void DoUploadData(final DocumentObject a){

        if (TextUtils.isEmpty(user.getAccessToken())){
            Intent i = new Intent(context, LoginActivity.class);
            context.startActivity(i);
            return;
        }


        Log.e("Salesniper","Album object: "+a.toString());

        AsyncTask.execute(new Runnable() {
            String res = "";
            int resCode = 0;
            @Override
            public void run() {

                try {
                    final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");

                    //MediaType ctype = MediaType.parse("application/json; charset=utf-8");
                    MultipartBody.Builder builder = new MultipartBody.Builder();

                     builder.setType(MultipartBody.FORM);

                    List<FileObject> pics = a.getCollections();
                    for (FileObject p :pics) {
                        File f = new File(p.getPix());
                        if (f.exists()){
                            builder.addFormDataPart("files[]",f.getName(), RequestBody.create(MEDIA_TYPE_PNG, f));
                        }
                    }

                    RequestBody requestBody = builder.build();


                    Request request = new Request.Builder()
                            .addHeader("Authorization","Bearer "+user.getAccessToken())
                            .url(UPLOAD_ENDPOINT)
                            .post(requestBody)
                            .build();

                    OkHttpClient client = new OkHttpClient();
                    Response response = client.newCall(request).execute();

                    resCode = response.code();
                    Log.e("Salesniper","Response code submission: "+resCode);
                    res = response.body().string();

                } catch (IOException e) {
                    resCode = -1;
                    e.printStackTrace();
                }

                if (resCode == 200 || resCode == 201 || resCode == 202){
                    Log.e("Salesniper","Outlet Submitted");
                    // TODO: 9/11/2016
                    //We set is uploaded to 1
                    a.setUploadStatus(true);
                    myDBHandler.UpdateDocument(a);
                    MainActivity.documentObjectsList = myDBHandler.GetUnUploaded();
                    //MainActivity.uploadedAlbumObjectList = myDBHandler.GetUploadedAlbums();

                }
                else if (resCode==-1){
                    // TODO: 9/11/2016
                    // we notify him that the job has been stacked due to time out
                    Log.e("Pixel","Timeout");
                }else{
                    // TODO: 9/11/2016
                    // we notify him that the job has been stacked
                    Log.e("Pixel","Error");
                }

                Log.e("Salesniper","Okhttp3: "+res);
            }
        });

    }


}
